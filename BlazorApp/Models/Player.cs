﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Models
{
    public class Player
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
    }
}

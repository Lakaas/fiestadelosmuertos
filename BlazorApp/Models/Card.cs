﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Models
{
    public class Card
    {
        public Card()
        {
            Guid = Guid.NewGuid();
            Word = string.Empty;
            Hints = new List<string>();
            IsWordVisible = false;
        }

        public Guid Guid { get; set; }
        public string Word { get; set; }
        public IList<string> Hints { get; set; }
        public bool IsWordVisible { get; set; }
    }
}
